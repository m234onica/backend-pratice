const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = mongoose.Schema.Types.ObjectId;
const CommentModelSchema = new Schema({
    postId:{
        type: ObjectId, 
        ref: 'post',
        require: true
    },
    author: {
        type: ObjectId,
        require: true,
        index: true,
        ref: 'User'
    },
    content: { type: String, require: true },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now },
    images:[String]

});

module.exports = mongoose.model('Comment', CommentModelSchema);
