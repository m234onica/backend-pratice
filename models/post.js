const mongoose = require('mongoose')
const Schema = mongoose.Schema
const ObjectId = mongoose.Schema.Types.ObjectId

const PostModelSchema = new Schema({
  content: { type: String, require: true },
  author: {
		type: ObjectId,
		require: true,
		index: true,
		ref: 'User' 
	},
	createdAt: { type: Date, default: Date.now },
	updatedAt: { type: Date, default: Date.now },
	images:[String]
})

module.exports = mongoose.model('Post', PostModelSchema)
