const _ = require('lodash');
const express = require('express')
const bodyPaser = require('body-parser')
const app = express()
app.use(bodyPaser.json())
const auth = require('./auth')
const port = process.env.PORT || 3000;

const aws = require('aws-sdk')
const multer = require('multer')
const multerS3 = require('multer-s3')
aws.config.loadFromPath('config/aws_config.json');

const s3 = new aws.S3({
  // params: {Bucket: bucketName}
})

var upload = multer({
  storage: multerS3({
    s3: s3,
    acl: 'public-read',
    bucket: 'backend-practice-joycehsu',
    metadata: function (req, file, cb) {
      cb(null, { fieldName: file.fieldname });
    },
    key: function (req, file, cb) {
      // cb(null, Date.now().toString())
      const ext = _.last(_.get(file, 'originalname', '').split('.')) || '';
      cb(null, file.fieldname + '-' + Date.now() + '.' + ext)
    }
  })
})

// var storage = multer.diskStorage({
//   destination: function (req, file, cb) {
//     cb(null, 'uploads')
//   },
//   filename: function (req, file, cb) {
//     const ext = _.last(_.get(file, 'originalname', '').split('.')) || '';
    
//     cb(null, file.fieldname + '-' + Date.now() + '.' + ext)
//   }
// })

// const upload = multer({ dest: 'uploads/'})

app.use(express.static('public'))
app.use('/uploads', express.static('uploads'))

require('./db')
require('./routes/user')(app)
require('./routes/post')(app, auth, upload)
require('./routes/comment')(app, auth, upload)

app.post('/upload', upload.single('file'),function(req, res){
  res.send('upload successed')
})

app.get('/', function (req, res) {
  res.send('hello world')
})
app.listen(port, function () {
  console.log('Express app listening on port', port)
})
