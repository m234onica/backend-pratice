const Post = require('../models/post')
const _ = require('lodash')
const htmlEncode = require('js-htmlencode')

module.exports = {
    getPost: getPost,
    getOnePost: getOnePost,
    createPost: createPost,
    deletedPost: deletedPost,
    updatedPost: updatedPost
}

async function getPost(req, res) {
    const posts = await Post.find({}).populate('author', 'username')
    res.send(posts)
}

async function getOnePost(req, res) {
    const post = await Post.findOne({
        _id: req.params.postId
    }).populate('author', 'username')
    res.send(post)
}

async function createPost(req, res) {
    console.log('files', req.files);
    console.log('body', req.body);
    const content = htmlEncode(req.body.content)

    const newPost = new Post({
        content: content,
        author: req.user._id,
        images: _.map(req.files,'location')
    })
    await newPost.save()
    res.send({ status: 'success', message: 'post created' })

}

async function deletedPost(req, res) {
    try {
        if (_.isEmpty(req.params.postId)) return res.send({ status: 'error', message: '請輸入文章ID' })
        //1. 取出該篇文章
        const post = await Post.findOne({ _id: req.params.postId })
        // console.log(post);

        if (_.isEmpty(post)) return res.send({ status: 'error', message: '找不到文章' })
        //2. 檢查文章作者是否為本人,若不是，回傳錯誤訊息

        // console.log(post.author);
        // console.log(req.user._id);
        // console.log(req.isCurrentUser(post.author));

        if (!req.isCurrentUser(post.author)) {
            return res.send({ status: 'error', message: '無修改權限' })
        }

        //3. 若是本人，更新文章內容
        Post.deleteOne(
            { _id: req.params.postId }
        ).exec()


        //4. 回傳成功訊息
        res.send({ status: 'success', message: 'post deleted!' })
    } catch (error) {
        console.log(error);
        res.send({ status: 'error', message: '未預期的錯誤' })
    }

}

async function updatedPost(req, res) {
    console.log(req.params);

    try {
        if (_.isEmpty(req.params.postId)) return res.send({ status: 'error', message: '請輸入文章ID' })
        //1. 取出該篇文章
        const post = await Post.findOne({ _id: req.params.postId })
        // console.log(post);

        if (_.isEmpty(post)) return res.send({ status: 'error', message: '找不到文章' })
        //2. 檢查文章作者是否為本人,若不是，回傳錯誤訊息

        if (!req.isCurrentUser(post.author)) {
            return res.send({ status: 'error', message: '無修改權限' })
        }

        //3. 若是本人，更新文章內容
        await post.update({
            content: req.body.content,
            updatedAt: new Date()
        }).exec()
        //4. 回傳成功訊息
        res.send({ status: 'success', message: 'post updated!' })
    } catch (error) {
        console.log(error);
        res.send({ status: 'error', message: '未預期的錯誤' })
    }
}


