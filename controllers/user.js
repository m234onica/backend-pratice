const _ = require('lodash')
const sha256 = require('js-sha256')
const moment = require('moment')
const User = require('../models/user')
const Session = require('../models/session')

module.exports = {
  createUser,
  login
}

async function createUser (req, res) {
  if (_.isEmpty(req.body.username || _.isEmpty(req.body.password))) {
    res.send({ status: 'error', message: '帳號密碼不得為空' })
  }
  // 檢查username是否重複
  const users = await User.find({ username: req.body.username })
  if (users.length > 0) return res.send({ status: 'error', message: '帳號已存在' })
  // 將密碼加密
  const encryptPassword = encryptString(req.body.password)

  const newUser = new User({
    username: req.body.username,
    password: encryptPassword
  })
  await newUser.save()
  res.send({ status: 'success', message: '註冊成功' })
}

async function login (req, res) {
  // 1. 檢查帳號密碼輸入
  if (_.isEmpty(req.body.username || _.isEmpty(req.body.password))) {
    res.send({ status: 'error', message: '帳號密碼不得為空' })
  }
  // 2. 到資料庫撈出此帳號的user
  const user = await User.findOne({ username: req.body.username })
  // 3. 若找不到user要回傳錯誤
  if (_.isEmpty(user)) res.send({ status: 'error', message: '找不到此帳號' })
  // 4. 檢查密碼
  // 將密碼加密
  // const encryptPassword = sha256.create();
  // encryptPassword.update(req.body.password);
  // encryptPassword.hex();
  const encryptPassword = encryptString(req.body.password)

  if (user.password !== encryptPassword) {
    return res.send({ status: 'error', message: '密碼錯誤' })
  }
  // 5. 產生session & token, 設定過期時間
  const newToken = encryptString(`${user._id.toString()}${new Date().toString()}`)
  const newSession = new Session({
    userId: user._id,
    expiredAt: moment().add(7, 'days'),
    token: newToken
  })
  newSession.save()
  // 6. 回傳結果＆token
  res.send({ status: 'success', message: '登入成功', token: newSession.token })
}

function encryptString (str) {
  // 將字串加密
  const encryptData = sha256.create()
  encryptData.update(str)
  encryptData.hex()
  return encryptData.toString()
}
