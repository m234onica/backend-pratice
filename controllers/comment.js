const Comment = require('../models/comment')
const Post = require('../models/post')
const _ = require('lodash')
const htmlEncode = require('js-htmlencode')


module.exports = {
    getpostComment: getpostComment,
    getoneComment: getoneComment,
    createComment: createComment,
    deletedComment: deletedComment,
    updatedComment: updatedComment
}

async function getpostComment(req, res) {
    const comment = await Comment.find({
        postId: req.params.postId
    }).populate('author','username')
    res.send(comment)
}

async function getoneComment(req, res) {
    const comment = await Comment.find({
        _id: req.params.commentId
    }).populate('author', 'username')
    res.send(comment)
}

async function createComment(req, res) {
    console.log('files', req.files);
    console.log('body', req.body);
    const content = htmlEncode(req.body.content)
    
    const newComment = new Comment({
        postId: req.params.postId,
        content: content,
        author: req.user._id,
        images: _.map(req.files, 'location')

    })
    await newComment.save()
    res.send('Comment create success!')
}

// async function deletedComment (req, res) {
//     Comment.findOneAndDelete(
//         { _id: req.params.commentId }
//     ).exec()
//     res.send('comment deleted')
// }

async function deletedComment(req, res) {
    try {
        if (_.isEmpty(req.params.postId)) return res.send({ status: 'error', message: '請輸入文章ID' })
        //1. 取出該篇文章
        const post = await Post.findOne({ _id: req.params.postId })

        if (_.isEmpty(post)) return res.send({ status: 'error', message: '找不到文章' })

        // console.log(post.author);
        // console.log(req.user._id);
        // console.log(req.isCurrentUser(post.author));
        //2. 取出該篇留言
        const comment = await Comment.findOne({ _id: req.params.commentId })
        if (_.isEmpty(comment)) return res.send({ status: 'error', message: '找不到留言' })

        //3. 檢查留言作者是否為本人,若不是，回傳錯誤訊息
        if (!req.isCurrentUser(comment.author)) {
            return res.send({ status: 'error', message: '無修改權限' })
        }

        //4. 若是本人，修改文章內容
        Comment.deleteOne(
            { _id: req.params.commentId }
        ) .exec()
        //5. 回傳成功訊息
        res.send({ status: 'success', message: 'post deleted!' })
    } catch (error) {
        console.log(error);        
        res.send({ status: 'error', message: '未預期的錯誤' })
    }

}

// async function updatedComment(req, res) {
//     Comment.findOneAndUpdate(
//         { _id: req.params.commentId },
//         {
//             '$set': {
//                 content: req.body.content,
//                 updatedAt: new Date()
//             }
//         }
//     ).exec()
//     res.send('comment updated')
// }

async function updatedComment(req, res) {
    try{
        if (_.isEmpty(req.params.postId)) return res.send({status: 'error', message: '請輸入文章ID'})
        //1. 取出該篇文章
        const post = await Post.findOne({_id: req.params.postId})
        
        if(_.isEmpty(post)) return res.send({ status:'error', message: '找不到文章'})

        //2. 取出該篇留言
        const comment = await Comment.findOne({_id: req.params.commentId})
        if (_.isEmpty(comment)) return res.send({ status: 'error', message: '找不到留言' })
        
        //3. 檢查留言作者是否為本人,若不是，回傳錯誤訊息
        if (!req.isCurrentUser(comment.author)) {
            return res.send({status: 'error',message: '無修改權限'})
        }

        //4. 若是本人，更新文章內容
        await comment.update({
            content: req.body.content,
            updatedAt: new Date()
        }).exec()
        //5. 回傳成功訊息
        res.send({status:'success', message: 'comment updated!'})
    } catch (error) {
        console.log(error);
        res.send({ status: 'error', message: '未預期的錯誤' })
    }
}
