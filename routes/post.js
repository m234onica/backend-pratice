
const postController = require('../controllers/post')

module.exports = function (app, auth, upload) {
  app.get('/post', postController.getPost)
  app.get('/post/:postId', postController.getOnePost)
  app.post('/post', auth, upload.array('images', 3), postController.createPost)
  app.delete('/post/:postId', auth, postController.deletedPost)
  app.put('/post/:postId', auth, postController.updatedPost)
}
 
