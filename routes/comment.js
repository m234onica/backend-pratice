const commentController = require('../controllers/comment')

module.exports = function(app, auth, upload){
    app.get('/post/:postId/comment', commentController.getpostComment)
    app.get('/post/:postId/comment/:commentId', commentController.getoneComment)
    app.post('/post/:postId/comment', auth, upload.array('images', 3), commentController.createComment)
    app.put('/post/:postId/comment/:commentId', auth, commentController.updatedComment)
    app.delete('/post/:postId/comment/:commentId', auth, commentController.deletedComment)
}
